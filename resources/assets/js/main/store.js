import Vue from 'vue';
import Vuex from 'vuex';
import ServerXHR from '../additional-plugins/ServerXHR';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        Laravel,
        backend_data : {
            home: {}
        },
        ServerXHR,
        requestIsBeingSent : false,
    },
    getters: {
        storeShowSpinner(state){
            return state.requestIsBeingSent;
        },
        Laravel(state){
            return state.Laravel;
        },
        state(state){
            return state;
        },
    },
    mutations: {
        change_backend_data(state, data) {
            state.backend_data[data.route_name] = data.backend_data;
        },
        changeStoreRequestIsBeingSent(state, requestIsBeingSent){
            state.requestIsBeingSent = requestIsBeingSent;
        },
    },

    actions: {
        change_backend_data({commit, state}, data) {
            commit('change_backend_data', data);
        },
        changeStoreRequestIsBeingSent({ commit, state }, requestIsBeingSent){
            commit('changeStoreRequestIsBeingSent', requestIsBeingSent);
        },
    }
});
