import VueRouter from 'vue-router';

const routes = [

    {
        path: '/:locale(' + Laravel.locales_chars.join('|') + ')?',
        component : process.BROWSER
            ? (resolve) => {
                require(['../components/Main/Index.vue'], resolve)
            }
            : require('../components/Main/Index.vue'),
        children: [
            {
                path: '/',
                name: 'home',
                component: process.BROWSER
                    ? (resolve) => {
                        require(['../components/Main/Views/Home.vue'], resolve)
                    }
                    : require('../components/Main/Views/Home.vue'),
            },
            {
                path: 'contact',
                name: 'contact',
                component: process.BROWSER
                    ? (resolve) => {
                        require(['../components/Main/Views/Contact.vue'], resolve)
                    }
                    : require('../components/Main/Views/Contact.vue'),
            },
        ]
    },


    {
        path: '*',
        name: 'not_found',
        component: process.BROWSER
            ? (resolve) => {
                require(['../components/Main/Views/404.vue'], resolve)
            }
            : require('../components/Main/Views/404.vue')
    },
];

export const router = new VueRouter({
    routes,
    linkActiveClass : 'active',
    mode : 'history',
    // base : '/'
});