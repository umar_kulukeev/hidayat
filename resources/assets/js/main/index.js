import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
import VueMask from 'vue-the-mask';
import { Validator } from 'vee-validate';
import { mapActions } from 'vuex';
import { mapGetters } from 'vuex';
require('../filter');


Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VeeValidate);

Validator.extend('truthy', {
    getMessage: field => 'The ' + field + ' value is not truthy.',
    validate: value => !!value && value != 0
});

if(process.BROWSER){
    Vue.use(VueMask);
    Vue.use(require('vue-scrollto'));
    Vue.component('datepicker', require('vue-flatpickr-component'));
    var Inputmask = require('inputmask');
    require('jquery-mask-plugin');
    require('.././additional-plugins/jquery.initialize');
    require('bootstrap-select');
    require('bootstrap-notify');
}


Vue.component(
    'app',
    require('./components/App.vue')
);
Vue.component(
    'spinner',
    require('../global_components/Spinner.vue')
);

let eventHub = new Vue();

Vue.mixin({
    data(){
        return {
            eventHub
        }
    },
    mounted(){

    },
    beforeRouteLeave(to, from, next){
        this.changeStoreRequestIsBeingSent(true);
        next();
    },
    beforeRouteEnter(to, from, next){
        next(
            vm => {
                vm.changeStoreRequestIsBeingSent(false);
            }
        )
    },
    methods : {
        trans($transSearch){

            let [ group, key ] = $transSearch.split('.');

            group = this.$store.state.Laravel.translations[Laravel.currentLocaleChar][group];

            if(!group || !group[key])
                return $transSearch;


            return group[key]
        },
        ...mapActions([
            'change_backend_data',
            'changeStoreRequestIsBeingSent'
        ])
    },
    computed : {
        ...mapGetters([
            'Laravel',
            'state',
            'storeShowSpinner'
        ])
    }
});

