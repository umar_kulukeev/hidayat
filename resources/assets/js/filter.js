import Vue from 'vue';
require('moment');

Vue.filter('formatDateMonth', (value) => {
    return moment(value).format("LL");
});
Vue.filter('truncate',(text,stop,clamp) =>{
	return text.slice(0,stop) +  (stop < text.length ? clamp || '...' : '');
});