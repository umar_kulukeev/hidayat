<div class="row">
    <div class="@if($create) col-md-12 @else col-md-8 @endif">
        <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
              data-ui-block-element="#superLargeModal .modal-body" id="ajaxForm">
            <ul class="nav nav-tabs" role="tablist">
                @foreach(config('project.locales') as $count => $locale)
                    <li role="presentation" class="nav-item">
                        <a class="@if($count == 0) active @endif nav-link" href="#tab-{{ $locale }}"
                           aria-controls="#tab-{{ $count }}" role="tab"
                           data-toggle="tab">{{ $locale }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(config('project.locales') as $count => $locale)
                    <div role="tabpanel" class="tab-pane @if($count == 0)  active  @endif" id="tab-{{ $locale }}">

                        <div class="form-group">
                            <label for="title.{{ $locale }}">Заголовок *</label>
                            <input type="text" class="form-control" id="title.{{ $locale }}"
                                   name="title[{{ $locale }}]"
                                   @if(isset($generalCategory)) value="{{ $generalCategory->getTranslation('title', $locale) }}" @endif>
                            <p class="help-block"></p>
                        </div>

                    </div>
                @endforeach
            </div>

            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="is_active"
                           @if(isset($generalCategory) && $generalCategory->getOriginal('is_active')) checked @endif>
                    Активен
                    <span></span>
                </label>
            </div>

            <div class="form-group  col-md-12">
                <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
            </div>
        </form>
    </div>
</div>
