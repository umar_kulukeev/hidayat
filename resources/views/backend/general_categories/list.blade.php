@foreach($generalCategories as $item)
    @include('backend.general_categories.item')
@endforeach

@if(!$generalCategories->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif