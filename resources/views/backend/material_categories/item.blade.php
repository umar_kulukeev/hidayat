<tr class="row-{{ $item->id }}">
    <td style="text-align: center">{{ $item->id }}</td>
    <td>{{$types[$item->type]}}</td>
    <td>{{ $item->title }}</td>
    <td style="text-align: center">{{ $item->isActive() }}</td>
    <td style="text-align: center">
        <a href="{{ route('admin.material.categories.edit', ['categoryId' => $item->id ]) }}" class="handle-click" data-type="modal" data-modal="#largeModal">
            <i class="la la-edit"></i>
        </a>
    </td>
</tr>