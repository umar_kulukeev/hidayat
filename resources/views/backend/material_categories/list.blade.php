@foreach($materialCategories as $item)
    @include('backend.material_categories.item')
@endforeach

@if(!$materialCategories->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif