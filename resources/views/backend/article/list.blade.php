@foreach($articles as $item)
    @include('backend.article.item')
@endforeach

@if(!$articles->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif