@foreach($audios as $item)
    @include('backend.audio.item')
@endforeach

@if(!$audios->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif