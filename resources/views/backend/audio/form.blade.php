<div class="row">
    <div class="@if($create) col-md-12 @else col-md-8 @endif">
        <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
              data-ui-block-element="#superLargeModal .modal-body" id="ajaxForm">
            <ul class="nav nav-tabs" role="tablist">
                @foreach(config('project.locales') as $count => $locale)
                    <li role="presentation" class="nav-item">
                        <a class="@if($count == 0) active @endif nav-link" href="#tab-{{ $locale }}"
                           aria-controls="#tab-{{ $count }}" role="tab"
                           data-toggle="tab">{{ $locale }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(config('project.locales') as $count => $locale)
                    <div role="tabpanel" class="tab-pane @if($count == 0)  active  @endif" id="tab-{{ $locale }}">

                        <div class="form-group">
                            <label for="title.{{ $locale }}">Заголовок *</label>
                            <input type="text" class="form-control" id="title.{{ $locale }}"
                                   name="title[{{ $locale }}]"
                                   @if(isset($audio)) value="{{ $audio->getTranslation('title', $locale) }}" @endif>
                            <p class="help-block"></p>
                        </div>

                        <div class="form-group">
                            <label for="intro_text.{{ $locale }}">Краткое описание</label>
                            <textarea rows="3" class="form-control" id="intro_text.{{ $locale }}"
                                      name="intro_text[{{ $locale }}]">@if(isset($audio)){{$audio->getTranslation('intro_text', $locale)}}@endif</textarea>
                            <p class="help-block"></p>
                        </div>

                        <div class="form-group">
                            <label for="full_text.{{ $locale }}">Полное описание</label>
                            <textarea rows="4" class="form-control editor" id="full_text.{{ $locale }}"
                                      name="full_text[{{ $locale }}]">@if(isset($audio)){{$audio->getTranslation('full_text', $locale)}}@endif</textarea>
                            <p class="help-block"></p>
                        </div>

                    </div>
                @endforeach
            </div>

            <div class="form-group">
                <label for="category_id">Категория</label>
                <select id="category_id" name="category_id" class="form-control">
                    <option value="">Выберите категорию</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"
                                @if(isset($audio) && $audio->category_id == $category->id) selected @endif>{{$category->title}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>

            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="is_active"
                           @if(isset($audio) && $audio->getOriginal('is_active')) checked @endif>
                    Активен
                    <span></span>
                </label>
            </div>

            <div class="form-group  col-md-12">
                <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
            </div>
        </form>
    </div>
</div>