@foreach($news as $item)
    @include('backend.news.item')
@endforeach

@if(!$news->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif