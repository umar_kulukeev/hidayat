@foreach($libraries as $item)
    @include('backend.audio.item')
@endforeach

@if(!$libraries->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif