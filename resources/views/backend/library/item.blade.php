<tr class="row-{{$item->id }}">
    <td style="text-align: center">{{ $item->id }}</td>
    <td align="auto">{{ $item->category->title }}</td>
    <td align="auto">{{ $item->title }}</td>
    <td align="auto">{{ $item->intro_text }}</td>
    <td align="auto">{!! $item->full_text !!}</td>
    <td class="text-center">{{ $item->isActive() }}</td>
    <td class="text-center">
        <a href="{{ route('admin.library.edit', ['id' => $item->id]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить продукт?"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{route('admin.library.destroy', ['id' => $item->id])}}">
            <i class="la la-trash"></i>
        </a>
    </td>
</tr>