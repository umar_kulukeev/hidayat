<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu"
            class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
            data-menu-vertical="true"
            data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
                <a href="{{route('admin.home')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Главная
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Контент
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>

            <li class="m-menu__item  @if(Request::is('admin/users*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="#" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Обратная связь</span>
					    </span>
					</span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::is('admin/article*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{ route('admin.article') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-add"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Статьи</span>
					    </span>
					</span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::is('admin/news*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{route('admin.news')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-notes"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Новости</span>
					    </span>
					</span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::is('admin/video*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{route('admin.video')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-imac"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Видео</span>
					    </span>
					</span>
                </a>
            </li>

            <li class="m-menu__item  @if(Request::is('admin/audio*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{ route('admin.audio') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-music"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Аудио</span>
					    </span>
					</span>
                </a>
            </li>

            <li class="m-menu__item  @if(Request::is('admin/photo*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{ route('admin.photo') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-profile"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Фото</span>
					    </span>
					</span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::is('admin/faq*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{ route('admin.faq') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-info"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Вопросы</span>
					    </span>
					</span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::is('admin/library*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="{{ route('admin.library') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-folder-2"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Библиотека</span>
					    </span>
					</span>
                </a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu
                @if(Request::is('admin/content/*')) m-menu__item--open m-menu__item--expanded @endif"
                aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-placeholder-2"></i>
                    <span class="m-menu__link-text">Мечети</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::is('admin/content/pages')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text ">Записи</span>
                            </a>
                        </li>

                        <li class="m-menu__item @if(Request::is('admin/content/news')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text">Заявки(1)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu
                @if(Request::is('admin/content/*')) m-menu__item--open m-menu__item--expanded @endif"
                aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-interface-2"></i>
                    <span class="m-menu__link-text">Каталог</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::is('admin/content/pages')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text ">Записи</span>
                            </a>
                        </li>

                        <li class="m-menu__item @if(Request::is('admin/content/news')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text">Заявки(2)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu
                @if(Request::is('admin/content/*')) m-menu__item--open m-menu__item--expanded @endif"
                aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-book"></i>
                    <span class="m-menu__link-text">Коран</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::is('admin/content/pages')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text ">Сура</span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::is('admin/content/news')) m-menu__item--active @endif" aria-haspopup="true" >
                            <a  href="#" class="m-menu__link ">
                                <span class="m-menu__link-text">Аят</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="m-menu__item  @if(Request::is('admin/users*'))m-menu__item--active @endif" aria-haspopup="true" >
                <a  href="#" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-clock-1"></i>
                    <span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Намаз</span>
					    </span>
					</span>
                </a>
            </li>

            <li class="m-menu__item  m-menu__item--submenu
            @if(Request::is('admin/*'))m-menu__item--open m-menu__item--expanded @endif"
                aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-search"></i>
                    <span class="m-menu__link-text">Справочники</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::is('admin/general-categories')) m-menu__item--active @endif"
                            aria-haspopup="true">
                            <a href="{{route('admin.general.categories')}}" class="m-menu__link ">
                                <span class="m-menu__link-text ">Общие категории</span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::is('admin/material-categories')) m-menu__item--active @endif"
                            aria-haspopup="true">
                            <a href="{{route('admin.material.categories')}}" class="m-menu__link ">
                                <span class="m-menu__link-text">Категории материалов</span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::is('admin/content/pages')) m-menu__item--active @endif"
                            aria-haspopup="true">
                            <a href="#" class="m-menu__link ">
                                <span class="m-menu__link-text ">Теги</span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::is('admin/content/news')) m-menu__item--active @endif"
                            aria-haspopup="true">
                            <a href="#" class="m-menu__link ">
                                <span class="m-menu__link-text">Адреса</span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::is('admin/content/news')) m-menu__item--active @endif"
                            aria-haspopup="true">
                            <a href="#" class="m-menu__link ">
                                <span class="m-menu__link-text">Авторы</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            @if($userAuth->canUse('settings_menu'))
                <li class="m-menu__item  m-menu__item--submenu
                    @if(Request::is('admin/settings/*')) m-menu__item--open m-menu__item--expanded @endif"
                    aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-cogwheel"></i>
                        <span class="m-menu__link-text">Настройки</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">

                            <li class="m-menu__item @if(Request::is('admin/settings/menu')) m-menu__item--active @endif" aria-haspopup="true" >
                                <a  href="{{route('admin.settings.menu')}}" class="m-menu__link ">
                                    <span class="m-menu__link-text">Управление меню</span>
                                </a>
                            </li>

                            <li class="m-menu__item @if(Request::is('admin/settings/menu/manage')) m-menu__item--active @endif" aria-haspopup="true" >
                                <a  href="{{route('admin.menu.manage')}}" class="m-menu__link ">
                                    <span class="m-menu__link-text">Настройка меню</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
            @endif

            {{--@if($userAuth->canUse('settings_seo'))--}}
                {{--<li class="m-menu__item  m-menu__item--submenu @if(Request::is('admin/seo/handle*'))--}}
                        {{--m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"--}}
                    {{--data-menu-submenu-toggle="hover">--}}
                    {{--<a  href="#" class="m-menu__link m-menu__toggle">--}}
                        {{--<i class="m-menu__link-icon flaticon-search"></i>--}}
                        {{--<span class="m-menu__link-text">Настройки SEO</span>--}}
                        {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                    {{--</a>--}}
                    {{--<div class="m-menu__submenu">--}}
                        {{--<span class="m-menu__arrow"></span>--}}
                        {{--<ul class="m-menu__subnav">--}}
                            {{--<li class="m-menu__item @if(Request::getQueryString() == 'part=main') m-menu__item--active @endif" aria-haspopup="true" >--}}
                                {{--<a  href="{{route('admin.settings.seo.handle',  ['part' => 'main'])}}" class="m-menu__link ">--}}
                                    {{--<span class="m-menu__link-text ">Главная страница</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}

                            {{--<li class="m-menu__item @if(Request::getQueryString() == 'part=news')) m-menu__item--active @endif" aria-haspopup="true" >--}}
                                {{--<a  href="{{route('admin.settings.seo.handle',  ['part' => 'news'])}}" class="m-menu__link ">--}}
                                    {{--<span class="m-menu__link-text ">Новости</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--@endif--}}
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>