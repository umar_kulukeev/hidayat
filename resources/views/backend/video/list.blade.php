@foreach($videos as $item)
    @include('backend.video.item')
@endforeach

@if(!$videos->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif