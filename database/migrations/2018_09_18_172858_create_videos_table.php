<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->json('title');
            $table->json('intro_text')->nullable();
            $table->json('full_text')->nullable();
            $table->date('publish_date')->nullable();
            $table->integer('views_count')->default(0);;
            $table->json('video_url')->nullable();
            $table->json('video_time')->nullable();
            $table->boolean('is_active')->default(0);;
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
