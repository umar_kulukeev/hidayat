<?php

return [
    'default_locale' => 'ru',
    'locales' => ['ru', 'ky', 'en'],
    'news_images_upload_path' => storage_path() . '/app/public/news_images',
    'media_upload_path' => storage_path() . '/app/public/uploaded_images',


    'localesData' => [
        [
            'locale' => 'ru',
            'desc' => 'Русский'
        ],

        [
            'locale' => 'ky',
            'desc' => 'Кыргызский'
        ],

        [
            'locale' => 'en',
            'desc' => 'Английский'
        ],
    ],

    'menus' => [
        'top' => [
            'title' => 'Верхнее меню',
            'slug' => ''
        ],

        'footer' => [
            'title' => 'Футер',
            'slug' => ''
        ]
    ],

    'category_types' => [
        'article' => 'Статья',
        'audio' => 'Аудио',
        'video' => 'Видео',
        'photo' => 'Фото',
        'library' => 'Библиотека',
        'faq' => 'Вопросы',
        'catalog' => 'Каталог',
        'news' => 'Новости'
    ],
];