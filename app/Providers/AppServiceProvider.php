<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\JSONApiResponse\JSONApiResponse;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Обработчик ответов в формате json
        $this->app->singleton('JSON', function()
        {
            return new JSONApiResponse();
        });
    }
}
