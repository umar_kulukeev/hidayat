<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\ResponseTrait;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialCategoryController extends Controller
{
    use ResponseTrait;

    /**
     * @var MaterialCategory
     */
    private $materialCategory;

    public function __construct(MaterialCategory $materialCategory)
    {
        $this->materialCategory = $materialCategory;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $types = config('project.category_types');

        return view('backend.material_categories.index', [
            'types' => $types,
            'title' => 'Категории материалов',
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getList(Request $request)
    {
        $materialCategories = $this->materialCategory->paginate(20);

        $types = config('project.category_types');

        return response()->json([
            'tableData' => view('backend.material_categories.list', [
                'materialCategories' => $materialCategories,
                'types' => $types,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $materialCategories->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create()
    {
        $types = config('project.category_types');

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Создание категории материалов',
            'modalContent' => view('backend.material_categories.form', [
                'create' => true,
                'types' => $types,
                'formAction' => route('admin.material.categories.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $materialCategory = $this->materialCategory->create($request->all());

        $types = config('project.category_types');

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#largeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.material_categories.item', ['item' => $materialCategory, 'types' => $types])->render()
        ]);
    }

    public function edit($categoryId)
    {
        $materialCategory = $this->materialCategory->find($categoryId);

        $types = config('project.category_types');

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Редактирование категорий материалов',
            'modalContent' => view('backend.material_categories.form', [
                'materialCategory' => $materialCategory,
                'create' => false,
                'types' => $types,
                'formAction' => route('admin.material.categories.update', ['categoryId' => $categoryId]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }

    public function update(Request $request, $categoryId)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $materialCategory = $this->materialCategory->find($categoryId);

        $materialCategory->update($request->all());

        $types = config('project.category_types');

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#largeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $categoryId,
            'content' => view('backend.material_categories.item', ['item' => $materialCategory, 'types' => $types])->render()
        ]);
    }
}
