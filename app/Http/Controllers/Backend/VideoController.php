<?php

namespace App\Http\Controllers\Backend;

use App\Models\MaterialCategory;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;

    /**
     * @var Video
     */
    private $video;

    public function __construct(Video $video, MaterialCategory $category)
    {
        $this->category = $category;
        $this->video = $video;
    }

    public function index()
    {
        return view('backend.video.index', ['title' => 'Видео']);
    }

    public function getList(Request $request)
    {
        $q = $this->video->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $video = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.video.list', [
                'videos' => $video,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $video->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'video', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание видео',
            'modalContent' => view('backend.video.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.video.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $video = $this->video->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.video.item', ['item' => $video])->render()
        ]);

    }

    public function edit($id)
    {
        $video = $this->video->find($id);

        $categories = $this->category->where(['type' => 'video', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование видео',
            'modalContent' => view('backend.video.form', [
                'video' => $video,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.video.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }


    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $video = $this->video->find($id);

        $video->update($request->all());

        $video = $this->video->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.video.item', ['item' => $video])->render()
        ]);
    }

    public function destroy($id)
    {
        $video = $this->video->find($id);
        $video->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
