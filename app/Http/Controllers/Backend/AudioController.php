<?php

namespace App\Http\Controllers\Backend;

use App\Models\Audio;
use App\Models\Category;
use App\Models\MaterialCategory;
use App\Services\CategoryService;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AudioController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;
    /**
     * @var Audio
     */
    private $audio;

    public function __construct(Audio $audio, MaterialCategory $category)
    {
        $this->category = $category;
        $this->audio = $audio;
    }

    public function index()
    {
        return view('backend.audio.index', ['title' => 'Аудио']);
    }

    public function getList(Request $request)
    {
        $q = $this->audio->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $audio = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.audio.list', [
                'audios' => $audio,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $audio->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'audio', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание аудио',
            'modalContent' => view('backend.audio.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.audio.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $audio = $this->audio->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#audioTable',
            'content' => view('backend.audio.item', ['item' => $audio])->render()
        ]);

    }

    public function edit($audioId)
    {
        $audio = $this->audio->find($audioId);

        $categories = $this->category->where(['type' => 'audio', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование аудио',
            'modalContent' => view('backend.audio.form', [
                'audio' => $audio,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.audio.update', ['audioId' => $audioId]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }


    public function update(Request $request, $audioId)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $audio = $this->audio->find($audioId);

        $audio->update($request->all());

        $audio = $this->audio->with('category')->find($audioId);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#audioTable',
            'row' => '.row-' . $audioId,
            'content' => view('backend.audio.item', ['item' => $audio])->render()
        ]);
    }

    public function destroy($audioId)
    {
        $audio = $this->audio->find($audioId);
        $audio->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#audioTable',
            'row' => '.row-' . $audioId,
        ]);
    }
}
