<?php

namespace App\Http\Controllers\Backend;

use App\Models\MaterialCategory;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;

    /**
     * @var News
     */
    private $news;

    public function __construct(News $news, MaterialCategory $category)
    {
        $this->category = $category;
        $this->news = $news;
    }

    public function index()
    {
        return view('backend.news.index', ['title' => 'Новости']);
    }

    public function getList(Request $request)
    {
        $q = $this->news->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $news = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.news.list', [
                'news' => $news,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $news->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'news', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание новостей',
            'modalContent' => view('backend.news.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.news.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $news = $this->news->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.news.item', ['item' => $news])->render()
        ]);

    }

    public function edit($id)
    {
        $news = $this->news->find($id);

        $categories = $this->category->where(['type' => 'news', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование новостей',
            'modalContent' => view('backend.news.form', [
                'news' => $news,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.news.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }


    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $news = $this->news->find($id);

        $news->update($request->all());

        $news = $this->news->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.news.item', ['item' => $news])->render()
        ]);
    }

    public function destroy($id)
    {
        $news = $this->news->find($id);
        $news->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
