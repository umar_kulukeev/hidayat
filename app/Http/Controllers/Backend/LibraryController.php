<?php

namespace App\Http\Controllers\Backend;

use App\Models\Library;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LibraryController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;
    /**
     * @var Library
     */
    private $library;

    public function __construct(Library $library, MaterialCategory $category)
    {
        $this->category = $category;
        $this->library = $library;
    }

    public function index()
    {
        return view('backend.library.index', ['title' => 'Библиотека']);
    }

    public function getList(Request $request)
    {
        $q = $this->library->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $library = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.library.list', [
                'libraries' => $library,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $library->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'library', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание библиотеки',
            'modalContent' => view('backend.library.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.library.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $library = $this->library->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.library.item', ['item' => $library])->render()
        ]);

    }

    public function edit($id)
    {
        $library = $this->library->find($id);

        $categories = $this->category->where(['type' => 'library', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование библиотеки',
            'modalContent' => view('backend.library.form', [
                'library' => $library,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.library.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }


    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $library = $this->library->find($id);

        $library->update($request->all());

        $library = $this->library->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.library.item', ['item' => $library])->render()
        ]);
    }

    public function destroy($id)
    {
        $library = $this->library->find($id);
        $library->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
