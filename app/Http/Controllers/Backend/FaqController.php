<?php

namespace App\Http\Controllers\Backend;

use App\Models\MaterialCategory;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;

    /**
     * @var Question
     */
    private $faq;

    public function __construct(Question $faq, MaterialCategory $category)
    {
        $this->category = $category;
        $this->faq = $faq;
    }

    public function index()
    {
        return view('backend.faq.index', ['title' => 'Вопросы']);
    }

    public function getList(Request $request)
    {
        $q = $this->faq->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $faq = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.faq.list', [
                'faqs' => $faq,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $faq->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'faq', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание вопроса',
            'modalContent' => view('backend.faq.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.faq.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $faq = $this->faq->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.faq.item', ['item' => $faq])->render()
        ]);

    }

    public function edit($id)
    {
        $faq = $this->faq->find($id);

        $categories = $this->category->where(['type' => 'faq', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование вопроса',
            'modalContent' => view('backend.faq.form', [
                'faq' => $faq,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.faq.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }

    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $faq = $this->faq->find($id);

        $faq->update($request->all());

        $faq = $this->faq->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.faq.item', ['item' => $faq])->render()
        ]);
    }

    public function destroy($id)
    {
        $faq = $this->faq->find($id);
        $faq->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
