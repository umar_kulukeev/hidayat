<?php

namespace App\Http\Controllers\Backend;

use App\Models\MaterialCategory;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;
    /**
     * @var Photo
     */
    private $photo;

    public function __construct(Photo $photo, MaterialCategory $category)
    {
        $this->category = $category;
        $this->photo = $photo;
    }

    public function index()
    {
        return view('backend.photo.index', ['title' => 'Фотографии']);
    }

    public function getList(Request $request)
    {
        $q = $this->photo->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $photo = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.photo.list', [
                'photos' => $photo,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $photo->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'photo', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание фотографий',
            'modalContent' => view('backend.photo.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.photo.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $photo = $this->photo->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.photo.item', ['item' => $photo])->render()
        ]);

    }

    public function edit($id)
    {
        $photo = $this->photo->find($id);

        $categories = $this->category->where(['type' => 'photo', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование фотографий',
            'modalContent' => view('backend.photo.form', [
                'photo' => $photo,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.photo.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }

    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $photo = $this->photo->find($id);

        $photo->update($request->all());

        $photo = $this->photo->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.photo.item', ['item' => $photo])->render()
        ]);
    }

    public function destroy($id)
    {
        $photo = $this->photo->find($id);
        $photo->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
