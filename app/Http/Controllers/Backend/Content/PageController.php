<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\ResponseTrait;
use App\Http\Requests\Backend\PagesRequest;
use App\Models\Media;
use App\Models\Page;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    use ResponseTrait;
    /**
     * @var Page
     */
    private $pages;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;

    public function __construct(Page $pages, Media $media, MediaService $mediaService)
    {
        $this->pages = $pages;
        $this->media = $media;
        $this->mediaService = $mediaService;
    }

    public function index()
    {
        return view('backend.content.pages.index', [
            'title' => 'Страницы',
        ]);
    }

    public function getList(Request $request)
    {
        $pages = $this->pages->orderBy('created_at', 'desc')->paginate(20);

        return response()->json([
            'tableData' => view('backend.content.pages.list', [
                'items' => $pages
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $pages->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);

    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание страницы',
            'modalContent' => view('backend.content.pages.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.content.pages.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }


    public function store(PagesRequest $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }

        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'content' => $request->content,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
        ];
//        dd($array);
        $page = $this->pages->create($array);

        if($request->has('videos') && $request->videos != null){
            $videos = explode("\r\n",$request->videos);
            foreach ($videos as $video) {
                $mediaService->uploadVideo($video,'pages', $page->id);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.pages.pages_item', ['page' => $page])->render()
        ]);


    }

    public function edit($pageId)
    {
        $foreign_languages = config('project.foreign_locales');
        $page = $this->pages->with('media')->find($pageId);
        $create = 0;
        $medias = $page->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.content.pages.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'page' => $page,
                'formAction' => route('admin.content.pages.update', ['id' => $pageId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(PagesRequest $request, $pageId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $page = $this->pages->find($pageId);
        $page->title = $request->input('title');
        $page->content = $request->input('content');
        $page->site_display = $site_display;
        $page->meta_description = $request->meta_description;
        $page->meta_keywords = $request->meta_keywords;
        $page->save();

        if($request->has('videos') && $request->videos != null){
            $videos = explode("\r\n",$request->videos);
            foreach ($videos as $video) {
                $mediaService->uploadVideo($video,'video', "pages", $pageId);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $pageId,
            'content' => view('backend.content.pages.pages_item', ['page' => $page])->render()
        ]);

    }

    public function media(Request $request, MediaService $mediaService, $pageId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "pages", $pageId, $thumbSizes);
        }

        $pages = $this->pages->whereHas('media', function ($query) use ($pageId) {
            $query->where('model_id', $pageId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.content.pages.media_list',
                [
                    'pages' => $pages,
                ])->render(),
        ]);
    }

    public function mainMedia($pageId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'pages')
            ->where('model_id', $pageId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'pages')->where('model_id', '=', $pageId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService,$mediaId)
    {

        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
        $mediaService->editVideo($image,$mediaId, $thumbSizes);

    }



    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'pages')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }

    public function destroy($pageId)
    {
        $medias = $this->media->where(['owner' => 'pages', 'model_id' => $pageId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $page = $this->pages->find($pageId);
        $page->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $pageId,
        ]);

    }
}
