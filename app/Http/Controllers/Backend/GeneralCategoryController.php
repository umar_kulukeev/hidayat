<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\ResponseTrait;
use App\Models\GeneralCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralCategoryController extends Controller
{
    use ResponseTrait;

    /**
     * @var GeneralCategory
     */
    private $generalCategory;

    public function __construct(GeneralCategory $generalCategory)
    {
        $this->generalCategory = $generalCategory;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.general_categories.index', [
            'title' => 'Общие категории',
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getList(Request $request)
    {
        $generalCategories = $this->generalCategory->paginate(20);

        return response()->json([
            'tableData' => view('backend.general_categories.list', [
                'generalCategories' => $generalCategories,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $generalCategories->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create()
    {
        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Создание общей категории',
            'modalContent' => view('backend.general_categories.form', [
                'create' => true,
                'formAction' => route('admin.general.categories.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $generalCategory = $this->generalCategory->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#largeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.general_categories.item', ['item' => $generalCategory])->render()
        ]);
    }

    public function edit($categoryId)
    {
        $generalCategory = $this->generalCategory->find($categoryId);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Редактирование общей категории',
            'modalContent' => view('backend.general_categories.form', [
                'generalCategory' => $generalCategory,
                'create' => false,
                'formAction' => route('admin.general.categories.update', ['categoryId' => $categoryId]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }

    public function update(Request $request, $categoryId)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $generalCategory = $this->generalCategory->find($categoryId);

        $generalCategory->update($request->all());

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#largeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $categoryId,
            'content' => view('backend.general_categories.item', ['item' => $generalCategory])->render()
        ]);
    }
}
