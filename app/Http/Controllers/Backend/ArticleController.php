<?php

namespace App\Http\Controllers\Backend;

use App\Models\Article;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * @var MaterialCategory
     */
    private $category;

    /**
     * @var Article
     */
    private $article;

    public function __construct(Article $article, MaterialCategory $category)
    {
        $this->category = $category;
        $this->article = $article;
    }

    public function index()
    {
        return view('backend.article.index', ['title' => 'Статьи']);
    }

    public function getList(Request $request)
    {
        $q = $this->article->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {

                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->whereRaw("LOWER(`title`->'$.\"ru\"') like '%$value%'");
                        }
                        break;
                    case 'is_active':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('is_active', $intVal);
                        }
                        break;
                }
            }
        }

        $article = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.article.list', [
                'articles' => $article,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $article->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $categories = $this->category->where(['type' => 'article', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание статьи',
            'modalContent' => view('backend.article.form', [
                'create' => true,
                'categories' => $categories,
                'formAction' => route('admin.article.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;
        $request->merge(['is_active' => $isActive]);

        $article = $this->article->create($request->all());

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.article.item', ['item' => $article])->render()
        ]);

    }

    public function edit($id)
    {
        $article = $this->article->find($id);

        $categories = $this->category->where(['type' => 'article', 'is_active' => 1])->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование статьи',
            'modalContent' => view('backend.article.form', [
                'article' => $article,
                'create' => false,
                'categories' => $categories,
                'formAction' => route('admin.article.update', ['id' => $id]),
                'buttonText' => 'Изменить',
            ])->render()
        ]);
    }


    public function update(Request $request, $id)
    {
        $isActive = ($request->has('is_active')) ? 1 : 0;

        $request->merge(['is_active' => $isActive]);

        $article = $this->article->find($id);

        $article->update($request->all());

        $article = $this->article->with('category')->find($id);

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
            'content' => view('backend.article.item', ['item' => $article])->render()
        ]);
    }

    public function destroy($id)
    {
        $article = $this->article->find($id);
        $article->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $id,
        ]);
    }
}
