<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Photo extends Model
{
    use HasTranslations;
    use SoftDeletes;

    protected $table = 'photos';

    protected $fillable = [
        'category_id',
        'title',
        'intro_text',
        'full_text',
        'publish_date',
        'views_count',
        'downloads_count',
        'file_url',
        'is_active',

    ];
    public $translatable = [
        'title',
        'intro_text',
        'full_text',
        'file_url',
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo(MaterialCategory::class, 'category_id');
    }

    public function isActive()
    {
        return ($this->is_active) ? 'Да' : 'Нет';
    }
}
