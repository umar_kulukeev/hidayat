<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Question extends Model
{
    use HasTranslations;
    use SoftDeletes;

    protected $table = 'questions';

    protected $fillable = [
        'category_id',
        'title',
        'question',
        'answer',
        'publish_date',
        'views_count',
        'is_active',

    ];
    public $translatable = [
        'title',
        'question',
        'answer',
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo(MaterialCategory::class, 'category_id');
    }

    public function isActive()
    {
        return ($this->is_active) ? 'Да' : 'Нет';
    }
}
