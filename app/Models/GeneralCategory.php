<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class GeneralCategory extends Model
{
    use HasTranslations;
    use SoftDeletes;

    protected $table = 'general_categories';

    protected $fillable = [
        'title',
        'is_active'
    ];

    public $translatable = [
        'title',
    ];

    protected $dates = ['deleted_at'];

    public function isActive()
    {
        return ($this->is_active) ? 'Да' : 'Нет';
    }
}
