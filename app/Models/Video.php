<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Video extends Model
{
    use HasTranslations;
    use SoftDeletes;

    protected $table = 'videos';

    protected $fillable = [
        'title',
        'intro_text',
        'full_text',
        'publish_date',
        'views_count',
        'video_url',
        'video_time',
        'is_active',
        'category_id'

    ];

    public $translatable = [
        'title',
        'intro_text',
        'full_text',
        'video_url',
        'video_time',
    ];

    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo(MaterialCategory::class, 'category_id');
    }

    public function isActive()
    {
        return ($this->is_active) ? 'Да' : 'Нет';
    }

}
