<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Tag extends Model
{
    use HasTranslations;
    use SoftDeletes;

    protected $table = 'tags';
    protected $fillable = ['name'];
    public $translatable = ['name'];

    protected $dates = ['deleted_at'];

    public function categories()
    {
        return $this->morphedByMany(Category::class, 'taggable');
    }
}
